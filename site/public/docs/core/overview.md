# overview

PubSweet is a modern, modular javascript toolkit for building publishing workflows.

> The PubSweet core is made up of two modules: [**`pubsweet-server`**](#server) and [**`pubsweet-client`**](#client), both of which can be extended and modified using [**components**](#client).

<h2 id="server">**pubsweet-server**</h2>

A nodeJS `express`-based app which runs on a server. This module contains the data models, handles the database layer, and exposes a REST API and optionally a realtime event stream using Server Sent Events.

<p style="text-align: right;">[...go to `pubsweet-server`](/docs/core/server.html)</p>

<h2 id="client">**pubsweet-client**</h2>

A React-based app which runs in the browser. It is written in nodeJS and intended to be bundled using Webpack. This module is the base for the user interface for a PubSweet app, interacting with the REST API of the server.

<p style="text-align: right;">[...go to `pubsweet-client`](/docs/core/client.html)</p>

<h2 id="components">components</h2>

Both server and client come with some essential functionality, but can be extended with components. PubSweet `client` components are ReactJS components, and can optionally include Redux actions and reducers. `server` components are express middleware functions. A component is a node module and can export a `client` component, a `server` component, or both.

By adding components you can rapidly include and customise features like:

- user management
- realtime sync
- team management
- advanced document editors
- format conversion

... and [much more](/docs/components/library.html).

<p style="text-align: right;">[...go to components](/docs/components/index.html)</p>

## A basic app

A basic PubSweet app is a nodeJS app that has `pubsweet-server` and `pubsweet-client` as dependencies, as well as:

- configuration for the app
- React scaffolding (HTML container, routes, etc.)
- WebPack configuration
- usually, some components

The PubSweet command-line interface [**`pubsweet`**](/docs/core/cli.html) helps with app development. It can generate a basic app, setup the database, run the app, and manage components.

<br>

**An example** of the basic structure of an app is [included in the CLI](https://gitlab.coko.foundation/pubsweet/pubsweet-cli/tree/master/initial-app).

<br>

---

### **Next:** take a closer look at [**`pubsweet-server`**](/docs/core/server.html).
