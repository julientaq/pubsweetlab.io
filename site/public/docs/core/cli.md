# command-line tool

## Getting PubSweet CLI

### Prerequisites

- node v7.6+
- npm v5+ or yarn v0.24+

We recommend using [`nvm`](https://github.com/creationix/nvm) to install and manage nodeJS and nvm versions.

### Installation

The PubSweet command-line tools can be installed from `npm` or `yarn`:

```bash
npm install --global pubsweet
```

or

```bash
yarn global add pubsweet
```

## Using PubSweet CLI

### Displaying the commands (`pubsweet` or `pubsweet help`)

Outputs:

```bash
Usage: pubsweet [options] [command]


Commands:

  new         create and set up a new pubsweet app
  run         start a pubsweet app
  setupdb     generate a database for a pubsweet app
  add         add one or more components to a pubsweet app
  adduser     add a user to the database for a pubsweet app
  help [cmd]  display help for [cmd]

Options:

  -h, --help     output usage information
  -V, --version  output the version number
```

For any command, you can see detailed usage help by running `pubsweet help cmd`, e.g.:

```bash
pubsweet help new
pubsweet help run
[...etc]
```

### Generating an app (`pubsweet new`)

The `new` subcommand generates a template PubSweet app for you to work from. This includes everything needed to run your publishing platform: dependencies, database setup, boilerplate code and configuration, and a set of initial components.

To generate an app, just provide the name:

```bash
pubsweet new myappname
```

`pubsweet` will create a subdirectory with the name you supplied, and start generating the app inside. It'll ask you a series of questions to customise your app. If you prefer to provide the answers in the initial command, you can do that instead:

```bash
pubsweet new myappname \
  --username someuser \
  --email some@email.com \
  --password correct-horse-battery-staple \
  --collection Articles
```

### Running your app (`pubsweet run`)

The `run` subcommand starts your app. It takes care of transpilation, module bundling and process management.

To start your app:

**either** use the `run` command from the app directory:

```bash
cd myappname
pubsweet run
```

**or** provide the app path to the `run` command:

```bash
pubsweet run path/to/myapp
```

### Setting up the database (`pubsweet setupdb`)

The `setupdb` subcommand creates the database for your app. It is usually used when you've cloned the source code of an existing app, or when you want to start over with a fresh database.

To generate a database for an app that doesn't have one yet:

```bash
pubsweet setupdb ./path/to/app/dir
```

By default this will generate a **production** database only. To generate a **dev** database, run the command with `--dev`:

```bash
pubsweet setupdb --dev ./path/to/app/dir
```

If your app already has a database, `pubsweet setupdb` will not overwrite it by default. You can force it to delete an existing database and overwrite it with a new one using `--clobber`:

```bash
$ pubsweet setupdb ./path/to/app/dir
info: Generating PubSweet app database at path api/db/production
error: Database appears to already exist
error: If you want to overwrite the database, use --clobber
$ pubsweet setupdb --clobber ./path/to/app/dir
info: Generating PubSweet app database at path api/db/production
info: Database appears to already exist
info: Overwriting existing database due to --clobber flag
info: setting up the database
info: building prompt
question:><Admin username><
[...etc]
```

As with `pubsweet new`, you can skip any or all of the interactive prompts by providing the user details with command-line flags:

```bash
pubsweet setupdb ./path/to/app/dir \
  --username someuser \
  --email some@email.com \
  --password correct-horse-battery-staple \
  --collection Articles
```

### Adding a user to the database (`pubsweet adduser`)

You can add a user to an existing database:

```bash
pubsweet adduser ./path/to/app/dir
```

You can optionally make that user an admin:

```bash
pubsweet adduser --admin ./path/to/app/dir
```

As with `pubsweet new` and `pubsweet setupdb`, you can skip any or all of the interactive prompts by providing the user details with command-line flags:

```bash
pubsweet adduser ./path/to/app/dir \
  --username someuser \
  --email some@email.com \
  --password correct-horse-battery-staple
```
