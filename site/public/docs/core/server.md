# Server


[![npm](https://img.shields.io/npm/v/pubsweet-server.svg.svg)](https://npmjs.com/package/pubsweet-server.svg)
[![MIT license](https://img.shields.io/badge/license-MIT-e51879.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-server.svg/raw/master/LICENSE)
[![code style standard](https://img.shields.io/npm/v/pubsweet-server.svg)](https://npmjs.com/package/pubsweet-server.svg)
[![coverage report](https://gitlab.coko.foundation/pubsweet/pubsweet-server/badges/master/coverage.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-server.svg/commits/master)
[![build status](https://gitlab.coko.foundation/pubsweet/pubsweet-server/badges/master/build.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-server.svg/commits/master)


> This module handles the [database layer](#database), contains the core [data models](#datamodels), and exposes a [REST API](#restapi)

<h2 id="database">Database layer</h2>

`pubsweet-server` supports [CouchDB](https://couchdb.apache.org/)-compatible storage layers - these are JSON document based noSQL stores that implement the CouchDB API.

In particular we support:

- [PouchDB](https://pouchdb.com) - basic local on-disk storage, useful for development and testing.
- [PouchDB-server](https://github.com/pouchdb/pouchdb-server) - a PouchDB-backed store with an HTTP API and management GUI.
- [CouchDB](https://couchdb.apache.org/) - fully distributable, hostable solution for production environments.

<h2 id="datamodels">Data models</h2>

The data models provided in `pubsweet-server` provide a foundation for most publishing applications:

- **knowledge** represented by [`Fragments`](#fragments) which can be organised in [`Collections`](#collections)
- **users** represented by [`Users`](#users) which can be organised in [`Teams`](#teams)
- **rules** governing how models may interact with one another represented by [`Authorize`](#authorize)

<h3 id="users">
  `User` model <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/pubsweet-server/blob/master/src/models/User.js)</small>
</h3>

#### example usage

```javascript
const User = require(`pubsweet-server/src/models/User`)

// create an admin user
const user = new User({
  username: 'admin',
  email: 'admin@website.net',
  password: 'correct-horse-battery-staple',
  admin: true
})

// save to the DB (this populates the object with its ID)
await user.save()

console.log('Saved admin user has ID:', user.id)
```

<h3 id="teams">
  `Team` model <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/pubsweet-server/blob/master/src/models/Team.js)</small>
</h3>

### example usage

...tbc

<h3 id="fragments">
  `Fragment` model <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/pubsweet-server/blob/master/src/models/Fragment.js)</small>
</h3>

### example usage

...tbc

<h3 id="collections">
  `Collection` model <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/pubsweet-server/blob/master/src/models/Collection.js)</small>
</h3>

#### example usage

```javascript
const Collection = require(`pubsweet-server/src/models/Collection`)

const title = 'articles'
const created = Date.now()

// create a new collection
const collection = new Collection({ title, created })

// set the owner of the collection
// requires that we have a previously saved User instance
collection.setOwners([user.id])

// save to the DB (this populates the object with its ID)
await collection.save()

console.log('Created collection with ID: ', collection.id)
```

<h3 id="authorize">
  `Authorize` model <small>[[code on gitlab]](https://gitlab.coko.foundation/pubsweet/pubsweet-server/blob/master/src/models/Authorize.js)</small>
</h3>

### example usage

...tbc

<h2 id="restapi">REST API</h2>
