<div style="width: 100%; text-align: center; margin-bottom: 4em">
  <h1>
    <code>pubsweet-client</code>
    </h1>

      <a href="https://npmjs.com/package/pubsweet-client">
        <img src="https://img.shields.io/npm/v/pubsweet-client.svg" alt="npm">
      </a>
      <a href="https://gitlab.coko.foundation/pubsweet/pubsweet-client/raw/master/LICENSE">
        <img src="https://img.shields.io/badge/license-MIT-e51879.svg" alt="MIT license">
      </a>
      <a href="https://standardjs.com/">
        <img src="https://img.shields.io/badge/code%20style-standard-green.svg" alt="code style standard">
      </a>
      <a href="https://gitlab.coko.foundation/pubsweet/pubsweet-client/commits/master">
        <img src="https://gitlab.coko.foundation/pubsweet/pubsweet-client/badges/master/build.svg" alt="build status">
      </a>
      <a href="https://gitlab.coko.foundation/pubsweet/pubsweet-client/commits/master">
        <img src="https://gitlab.coko.foundation/pubsweet/pubsweet-client/badges/master/coverage.svg" alt="coverage report">
      </a>
</div>

> This module creates the app that runs in the user's browser. It handles the [user interface](#ui), [routing](#routing), and optionally [state management](#state).

<h2 id="ui">User interface</h2>

`pubsweet-client` is a [React](https://facebook.github.io/react/) app. In general any React components should *just work* in a PubSweet app.

The PubSweet client uses [JSX](https://facebook.github.io/react/docs/introducing-jsx.html). This means the client must be compiled to plain JS. However, a PubSweet app in general can be extended [without using JSX](https://facebook.github.io/react/docs/react-without-jsx.html), so you don't need to learn JSX to use PubSweet.

<h2 id="routing">Routing</h2>

`pubsweet-client` uses [`react-router`](https://github.com/reacttraining/react-router) for routing between views of an app.

By convention we store routes in [`app/routes.jsx`](https://gitlab.coko.foundation/pubsweet/pubsweet-cli/blob/master/initial-app/app/routes.jsx).

Adding or modifying routes involves simply editing the `routes.jsx` file.

<h2 id="state">State management</h2>

[Redux](http://redux.js.org/) is used for app-wide state management. Redux uses `actions` to handle spawning state-changing effects, and `reducers` to handle updating the state based on the outcome of `actions`.

`pubsweet-client` includes some [`actions`](https://gitlab.coko.foundation/pubsweet/pubsweet-client/tree/master/src/actions) and [`reducers`](https://gitlab.coko.foundation/pubsweet/pubsweet-client/tree/master/src/reducers) for sending information to the `pubsweet-server` REST API and applying the results to the redux state.

Any component can also manage its own state using internal variables, and choose not to interact with the Redux store at all.
