# Projects using PubSweet

## Editoria

[Editoria](https://gitlab.coko.foundation/yannisbarlas/editoria) is a book editing platform built in collaboration with the University of California Press by COKO foundation, and in particular by the Editoria team based in Athens.

### Editoria resources

- [Source code repository](https://gitlab.coko.foundation/editoria/editoria)
- [Issue tracker](https://gitlab.coko.foundation/editoria/editoria/issues)
- [Discussion room](https://mattermost.coko.foundation/coko/channels/editoria)
